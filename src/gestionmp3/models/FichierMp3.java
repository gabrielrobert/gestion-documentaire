
package gestionmp3.models;

import java.util.List;

/**
 * Représentation objet d'un fichier mp3. Utilisé pour le mapping code/xml
 * 
 * @author grobert
 * 
 */
public class FichierMp3
{
	/**
	 * Titre du fichier mp3
	 */
	private String titre;

	/**
	 * Durée en mm:ss du fichier mp3
	 */
	private String duree;

	/**
	 * Chemin absolu pointant vers le fichier Mp3. C'est quelque peu une clef
	 * unique.
	 */
	private String repertoire;

	/**
	 * Liste d'artistes
	 */
	private String[] artistes;

	public FichierMp3()
	{
		this(null, null, null, null);
	}

	/**
	 * Constructeur de l'object FichierMp3
	 * 
	 * @param pTitre String, représentation du titre du fichier MP3
	 * @param pDuree String, représentation textuelle de la durée du fichier MP3
	 */
	public FichierMp3(String pTitre, String pDuree, String pRepertoire)
	{
		this(pTitre, pDuree, null, pRepertoire);
	}

	/**
	 * Constructeur de l'object FichierMp3
	 * 
	 * @param pTitre String, représentation du titre du fichier MP3
	 * @param pDuree String, représentation textuelle de la durée du fichier MP3
	 * @param pArtistes List<String>, représentation textuelle de l'ensemble des
	 *            artistes
	 */
	public FichierMp3(String pTitre, String pDuree, String[] pArtistes,
			String pRepertoire)
	{
		setTitre(pTitre);
		setDuree(pDuree);
		setArtistes(pArtistes);
		setRepertoire(pRepertoire);
	}

	/**
	 * Getter du titre du fichier MP3
	 * 
	 * @return String, titre du fichier MP3
	 */
	public String getTitre()
	{
		return titre;
	}

	/**
	 * Setter du titre du fichier MP3
	 * 
	 * @param titre String, titre du fichier MP3
	 */
	public void setTitre(String titre)
	{
		this.titre = titre;
	}

	/**
	 * Getter de la durée du fichier MP3
	 * 
	 * @return String, durée du fichier MP3
	 */
	public String getDuree()
	{
		return duree;
	}

	/**
	 * Setter de la durée du fichier MP3
	 * 
	 * @param duree String, durée du fichier MP3
	 */
	public void setDuree(String duree)
	{
		this.duree = duree;
	}

	/**
	 * Getter d'un tableau d'artistes du fichier MP3
	 * 
	 * @return List<String>, liste des artistes du fichier MP3
	 */
	public String getArtistes()
	{
		String strArtistes = new String();

		for (int i = 0; i < this.artistes.length; i++)
		{
			if (i == 0)
			{
				strArtistes = this.artistes[i];
			}
			else
			{
				strArtistes = String.format("%s feat %s", strArtistes,
						artistes[i]);
			}
		}

		return strArtistes;
	}

	/**
	 * Setter du tableau d'artistes du fichier MP3
	 * 
	 * @param artistes List<String>, représentation des artistes du fichier MP3
	 */
	public void setArtistes(String[] artistes)
	{
		this.artistes = artistes;
	}

	/**
	 * Getter du répertoire (sous forme absolue)
	 */
	public String getRepertoire()
	{
		return this.repertoire;
	}

	/**
	 * Setter du répertoire (sous forme absolue)
	 */
	public void setRepertoire(String repertoire)
	{
		this.repertoire = repertoire;
	}

	public String toString()
	{
		return String.format("%s - %s (%s)", this.getArtistes(),
				this.getTitre(), this.getDuree());
	}
}
