
package gestionmp3;

import java.io.File;
import java.util.ArrayList;

import gestionmp3.logic.XmlToMp3;
import gestionmp3.models.FichierMp3;

public class TestDivers
{

	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args)
	{
		File fichier = new File("src/gestionmp3/xml/mp3.xml");
		XmlToMp3 xmlHandler = new XmlToMp3(fichier);
		xmlHandler.AfficherDocument();
		ArrayList<FichierMp3> test = xmlHandler.extraireFichierMp3();
		System.out.println(test.get(0).getRepertoire());
	}

}
