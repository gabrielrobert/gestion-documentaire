
package gestionmp3.gui;

import gestionmp3.models.FichierMp3;
import gestionmp3.utils.Enums;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class BoiteDialogue extends JDialog implements ActionListener
{
	private boolean cancelled;

	public BoiteDialogue(JFrame parent, String title, String message)
	{
		super(parent, title, true);
		if (parent != null)
		{
			Dimension parentSize = parent.getSize();
			Point p = parent.getLocation();
			setLocation(p.x + parentSize.width / 4, p.y + parentSize.height / 4);
		}

		JPanel messagePane = new JPanel();
		messagePane.add(new JLabel(message));
		messagePane.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
		this.getContentPane().add(messagePane);

		JPanel buttonPane = new JPanel();

		JButton btnConfirmer = new JButton("Confirmer");
		JButton btnAnnuler = new JButton("Annuler");

		btnConfirmer.addActionListener(this);
		btnAnnuler.addActionListener(this);

		btnConfirmer.setActionCommand("confirmer");
		btnAnnuler.setActionCommand("annuler");

		buttonPane.add(btnConfirmer);
		buttonPane.add(btnAnnuler);

		this.getContentPane().add(buttonPane, BorderLayout.SOUTH);

		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.pack();
		this.setVisible(true);
	}

	public BoiteDialogue(JFrame parent, String title, String message,
			FichierMp3 fichier)
	{
		super(parent, title, true);
		if (parent != null)
		{
			Dimension parentSize = parent.getSize();
			Point p = parent.getLocation();
			setLocation(p.x + parentSize.width / 4, p.y + parentSize.height / 4);
		}

		JPanel messagePane = new JPanel();
		messagePane.add(new JLabel(message));
		messagePane.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));

		JPanel buttonPane = new JPanel();
		JPanel modificationPane = new JPanel();
		buttonPane.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));

		JButton btnConfirmer = new JButton("Confirmer");
		JButton btnAnnuler = new JButton("Annuler");
		JTextField txtTitre = new JTextField(fichier.getTitre());

		btnConfirmer.addActionListener(this);
		btnAnnuler.addActionListener(this);

		btnConfirmer.setActionCommand("confirmer");
		btnAnnuler.setActionCommand("annuler");

		buttonPane.add(btnConfirmer);
		buttonPane.add(btnAnnuler);
		modificationPane.add(txtTitre);

		this.getContentPane().add(messagePane, BorderLayout.NORTH);
		this.getContentPane().add(modificationPane, BorderLayout.CENTER);
		this.getContentPane().add(buttonPane, BorderLayout.SOUTH);

		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.pack();
		this.setVisible(true);

	}

	public boolean isCancelled()
	{
		return this.cancelled;
	}

	public void actionPerformed(ActionEvent e)
	{
		String command = e.getActionCommand();

		if (command.equals("confirmer"))
		{
			this.cancelled = false;
			setVisible(false);
			dispose();
		}
		else if (command.equals("annuler"))
		{
			this.cancelled = true;
			setVisible(false);
			dispose();
		}
	}
}
