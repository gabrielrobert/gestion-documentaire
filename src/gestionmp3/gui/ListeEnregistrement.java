
package gestionmp3.gui;

import gestionmp3.logic.XmlToMp3;
import gestionmp3.models.FichierMp3;
import gestionmp3.utils.Enums;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class ListeEnregistrement extends JPanel implements
		ListSelectionListener
{
	public static XmlToMp3 mp3Loader = new XmlToMp3(new File(
			"src/gestionmp3/xml/mp3.xml"));
	public static final String FRAME_TITLE = "Gestion Mp3 - Lister les enregistrements";

	private static final String BTN_SUPPRIMER_LABEL = "Supprimer";
	private static final String BTN_AJOUTER_LABEL = "Ajouter";
	private static final String BTN_MODIFIER_LABEL = "Modifier";
	private static final int NB_MP3_AFFICHAGE = 20;

	private JFrame parent;

	// Déclaration de la liste
	private JList jList;
	private DefaultListModel listModel;

	// Déclarations des boutons
	private JButton btnAjouter;
	private JButton btnModifier;
	private JButton btnSupprimer;

	public ListeEnregistrement(JFrame frame)
	{
		super(new BorderLayout());
		this.parent = frame;
		this.updateData();

		// Création de la liste
		jList = new JList(listModel);
		jList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		jList.setSelectedIndex(0);
		jList.addListSelectionListener(this);
		jList.setVisibleRowCount(NB_MP3_AFFICHAGE);
		// Utilisation de la liste dans un JScrollPane
		JScrollPane listScrollPane = new JScrollPane(jList);

		// Création des boutons
		btnSupprimer = new JButton(BTN_SUPPRIMER_LABEL);
		btnSupprimer.setActionCommand(BTN_SUPPRIMER_LABEL);

		btnAjouter = new JButton(BTN_AJOUTER_LABEL);
		btnAjouter.setActionCommand(BTN_AJOUTER_LABEL);

		btnModifier = new JButton(BTN_MODIFIER_LABEL);
		btnModifier.setActionCommand(BTN_MODIFIER_LABEL);

		String name = listModel.getElementAt(jList.getSelectedIndex())
				.toString();

		// Création du panel principal
		JPanel buttonsPanel = this.genererPanel();

		// Mise en place des action listener
		btnSupprimer.addActionListener(new BtnSupprimerListener());
		btnAjouter.addActionListener(new BtnAjouterListener());
		btnModifier.addActionListener(new BtnModifierListener());

		add(listScrollPane, BorderLayout.CENTER);
		add(buttonsPanel, BorderLayout.PAGE_END);
	}

	/**
	 * Générer le panneau de contrôle des boutons. Fonction dépendante des
	 * boutons
	 * 
	 * @return JPanel correctement structuré
	 */
	private JPanel genererPanel()
	{
		JPanel buttonPane = new JPanel();

		// Mise en place du bouton d'ajout
		buttonPane.add(this.btnAjouter);
		buttonPane.add(Box.createHorizontalStrut(5));
		buttonPane.add(new JSeparator(SwingConstants.VERTICAL));
		buttonPane.add(Box.createHorizontalStrut(5));

		// Mise en place du bouton de modification
		buttonPane.add(this.btnModifier);
		buttonPane.add(Box.createHorizontalStrut(5));
		buttonPane.add(new JSeparator(SwingConstants.VERTICAL));
		buttonPane.add(Box.createHorizontalStrut(5));

		// Mise en place du bouton pour supprimer un enregistrement
		buttonPane.add(this.btnSupprimer);

		buttonPane.setLayout(new BoxLayout(buttonPane, BoxLayout.LINE_AXIS));
		buttonPane.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));

		return buttonPane;
	}

	void mettreAJour(int index)
	{
		this.updateData();
		this.updateGui(index);
	}

	private void updateData()
	{
		// Récupérer les données
		ArrayList<FichierMp3> listeMp3 = mp3Loader.extraireFichierMp3();

		// Ajouter les éléments à la liste
		listModel = new DefaultListModel();
		for (FichierMp3 fichier : listeMp3)
		{
			listModel.addElement(fichier);
		}

		if (this.jList == null)
		{
			this.jList = new JList(listModel);
		}
		else
		{
			this.jList.setModel(listModel);
		}
	}

	private void updateGui(int index)
	{
		int size = listModel.getSize();

		if (size == 0)
		{ // Nobody's left, disable firing.
			btnAjouter.setEnabled(false);
		}
		else
		{ // Select an index.
			if (index == listModel.getSize())
			{
				// removed item in last position
				index--;
			}

			jList.setSelectedIndex(index);
			jList.ensureIndexIsVisible(index);
		}
	}

	class BtnAjouterListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			// Création d'un fichier de type FichierMp3 à partir de l'index
			int index = jList.getSelectedIndex();
			FichierMp3 test = (FichierMp3) listModel.get(index);

			listModel.remove(index);

			ListeEnregistrement.this.updateGui(index);

			int size = listModel.getSize();

			if (size == 0)
			{ // Nobody's left, disable firing.
				btnAjouter.setEnabled(false);
			}
			else
			{ // Select an index.
				if (index == listModel.getSize())
				{
					// removed item in last position
					index--;
				}

				jList.setSelectedIndex(index);
				jList.ensureIndexIsVisible(index);
			}
		}
	}

	class BtnModifierListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			// Création d'un fichier de type FichierMp3 à partir de l'index
			int index = jList.getSelectedIndex();
			FichierMp3 fichier = (FichierMp3) listModel.get(index);
			
			BoiteDialogue dlgSupprimer = new BoiteDialogue(
					ListeEnregistrement.this.parent,
					Enums.Titres.MODIFIER.toString(),
					Enums.Messages.MODIFIER.toString(),
					fichier);
			
			if (!dlgSupprimer.isCancelled())
			{
				// Réécriture du fichier XML
				if (mp3Loader.supprimerFichier(fichier))
				{
					listModel.remove(index);
				}

				ListeEnregistrement.this.mettreAJour(index);

			}
		}
	}

	class BtnSupprimerListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			BoiteDialogue dlgSupprimer = new BoiteDialogue(
					ListeEnregistrement.this.parent,
					Enums.Titres.SUPPRIMER.toString(),
					Enums.Messages.AJOUTER.toString());
			
			if (!dlgSupprimer.isCancelled())
			{
				// Création d'un fichier de type FichierMp3 à partir de l'index
				int index = jList.getSelectedIndex();
				FichierMp3 fichier = (FichierMp3) listModel.get(index);

				// Réécriture du fichier XML
				if (mp3Loader.supprimerFichier(fichier))
				{
					listModel.remove(index);
				}

				ListeEnregistrement.this.mettreAJour(index);

			}

		}
	}

	// This method is required by ListSelectionListener.
	public void valueChanged(ListSelectionEvent e)
	{
		if (e.getValueIsAdjusting() == false)
		{

			if (jList.getSelectedIndex() == -1)
			{
				// No selection, disable fire button.
				btnAjouter.setEnabled(false);

			}
			else
			{
				// Selection, enable the fire button.
				btnAjouter.setEnabled(true);
			}
		}
	}
}
