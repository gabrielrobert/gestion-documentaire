
package gestionmp3.utils;

public class Enums
{
	public enum Messages
	{
		SUPPRIMER("Êtes-vous sur de vouloir supprimer cette séquence?"),
		AJOUTER("Désirez-vous ajouter cet item ?"),
		MODIFIER("Êtes-vous sur de vouloir modifier cette séquence?");

		private String message;

		Messages(String message)
		{
			this.message = message;
		}

		public String toString()
		{
			return this.message;
		}
	}

	public enum Titres
	{
		SUPPRIMER("Supprimer cet item"), 
		AJOUTER("Ajouter cet item"),
		MODIFIER("Modifier cet item");

		private String titre;

		Titres(String titre)
		{
			this.titre = titre;
		}

		public String toString()
		{
			return this.titre;
		}
	}
}
