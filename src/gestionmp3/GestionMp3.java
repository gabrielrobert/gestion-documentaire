
package gestionmp3;

import gestionmp3.gui.ListeEnregistrement;
import gestionmp3.logic.XmlToMp3;
import gestionmp3.models.FichierMp3;

import java.awt.*;
import java.awt.event.*;

import java.io.File;
import java.util.ArrayList;

import javax.swing.*;
import javax.swing.event.*;

public class GestionMp3
{


	/**
	 * Créer l'interface graphique et permettre son affichage à l'écran
	 */
	private static void init()
	{
		// Création du frame
		JFrame frame = new JFrame(ListeEnregistrement.FRAME_TITLE);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// Création des composants essentiels.
		JComponent mainPanel = new ListeEnregistrement(frame);
		mainPanel.setOpaque(true); // content panes must be opaque
		frame.setContentPane(mainPanel);

		// Affichage du frame
		frame.pack();
		// L'affichage de l'application se fera au centre de l'écran
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}

	public static void main(String[] args)
	{
		javax.swing.SwingUtilities.invokeLater(new Runnable()
		{
			public void run()
			{
				init();
			}
		});
	}
}
