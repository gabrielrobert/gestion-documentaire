
package gestionmp3.logic;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import org.jdom2.*;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import gestionmp3.models.FichierMp3;

;

/**
 * Cette classe utilitaire est utilisé pour traiter les fichiers XML. Elle
 * permet l'ouverture, la fermeture, la sauvegarde et finalement la lecture
 * 
 * @author grobert
 * 
 */
public class XmlToMp3
{
	/**
	 * Propriété qui pointe vers le fichier, sous forme de path
	 */
	private File fichierXml;

	/**
	 * Document XML préalablement parsé
	 */
	private Document documentXml;

	/**
	 * Représentation de la node "root" du fichier XML
	 */
	private Element racine;

	/**
	 * Constructeur du XmlHander, il permet d'affecter les différentes valeurs
	 * de base.
	 * 
	 * @param pathFichier String, représentant le path qui pointe vers le
	 *            fichier
	 * @throws Exception, si le document donné est invalide
	 */
	public XmlToMp3(File fichier)
	{
		setPathFichier(fichier);
		setDocumentXml(ParseXml());
		setRacine(this.getDocumentRoot());
	}

	/**
	 * Afficher un document XML sous forme de System.out.println
	 */
	public void AfficherDocument()
	{
		Element racine = this.getRacine();
		List listeMp3 = racine.getChildren("mp3");
		Iterator mp3Node = listeMp3.iterator();

		while (mp3Node.hasNext())
		{
			Element courant = (Element) mp3Node.next();
			Iterator enregistrementNode = courant.getChildren("enregistrement")
					.iterator();

			while (enregistrementNode.hasNext())
			{
				Element enregistrementCourant = (Element) enregistrementNode
						.next();
				System.out.println(enregistrementCourant.getChild("titre")
						.getText());
			}
		}
	}

	/**
	 * Générer des objets FichierMp3 à partir de la lecture d'un document XML
	 * 
	 * @return ArrayList<FichierMp3>, représentant l'ensemble des fichiers MP3
	 *         d'un document XML
	 */
	public ArrayList<FichierMp3> extraireFichierMp3()
	{
		ArrayList<FichierMp3> fichiersMp3 = new ArrayList<FichierMp3>();

		Element racine = this.getRacine();
		Iterator mp3Node = racine.getChildren("mp3").iterator();

		// Iteration sur le deuxième niveau, tag <mp3>
		while (mp3Node.hasNext())
		{
			Element mp3Courant = (Element) mp3Node.next();
			Iterator enregistrementNode = mp3Courant.getChildren(
					"enregistrement").iterator();

			// Iteration sur le troisième niveua, tag <enregistrement>
			while (enregistrementNode.hasNext())
			{
				Element enregistrementCourant = (Element) enregistrementNode
						.next();
				fichiersMp3.add(new FichierMp3(enregistrementCourant.getChild(
						"titre").getText(), enregistrementCourant.getChild(
						"duree").getText(), this
						.extraireArtiste(enregistrementCourant
								.getChild("artistes")), enregistrementCourant
						.getChild("repertoire").getText()));

			}
		}

		return fichiersMp3;
	}

	public boolean supprimerFichier(FichierMp3 fichier)
	{

		Element racine = this.getRacine();
		Iterator mp3Node = racine.getChildren("mp3").iterator();

		// Iteration sur le deuxième niveau, tag <mp3>
		while (mp3Node.hasNext())
		{
			Element mp3Courant = (Element) mp3Node.next();
			Iterator enregistrementNode = mp3Courant.getChildren("enregistrement").iterator();

			// Iteration sur le troisième niveua, tag <enregistrement>
			while (enregistrementNode.hasNext())
			{
				Element enregistrementCourant = (Element) enregistrementNode.next();
				
				if (enregistrementCourant.getChild("repertoire").getText().equals(fichier.getRepertoire()))
				{
					enregistrementNode.remove();
				}
			}
		}
		
		return this.enregistre();
	}

	public boolean enregistre()
	{
		boolean fail = false;
		try
		{
			XMLOutputter sortie = new XMLOutputter(Format.getPrettyFormat());
			sortie.output(documentXml, new FileOutputStream(this.fichierXml.getAbsolutePath()));
		}
		catch (java.io.IOException e)
		{
			fail = true;
		}
		
		return fail;
	}

	/**
	 * Extraire les artistes d'une node <artistes>
	 * 
	 * @param nodeSource
	 * @return String[], la représentation textuelle des artistes
	 */
	private String[] extraireArtiste(Element nodeSource)
	{
		ArrayList<String> artistes = new ArrayList<String>();
		Iterator artistesNode = nodeSource.getChildren("artiste").iterator();

		while (artistesNode.hasNext())
		{
			Element artisteCourant = (Element) artistesNode.next();
			artistes.add(artisteCourant.getText());
		}

		return artistes.toArray(new String[artistes.size()]);
	}

	/**
	 * Getter de la propriété pathFichier
	 * 
	 * @return String, représentation du path du fichier
	 */
	public File getFichierXml()
	{
		return this.fichierXml;
	}

	/**
	 * Setter de la propriété pathFichier
	 * 
	 * @param pathFichier, string respresentant le path du fichier xml
	 */
	public void setPathFichier(File fichier)
	{
		this.fichierXml = fichier;
	}

	/**
	 * Getter de la propriété du document xml parsé
	 * 
	 * @return org.jdom2.Document, document parsé par SAX Builder
	 */
	public Document getDocumentXml()
	{
		return documentXml;
	}

	/**
	 * Setter du document xml parsé
	 * 
	 * @param documentXml, document préalablement parsé par SAX builder
	 */
	public void setDocumentXml(Document documentXml)
	{
		this.documentXml = documentXml;
	}

	/**
	 * Retourner l'élément root du document
	 * 
	 * @return Element, node root du document
	 */
	public Element getRacine()
	{
		return this.racine;
	}

	/**
	 * Setter de la propriété "racine", c'est le root du document xml
	 * 
	 * @param racine Element, node "root" du document xml
	 */
	public void setRacine(Element racine)
	{
		this.racine = racine;
	}

	/**
	 * Parser un fichier de type XML
	 * 
	 * @return Document, fichier XML parsé par SAXBuilder
	 * @throws Exception, lorsque le document n'est pas valide/trouvable
	 */
	private Document ParseXml()
	{
		Document document = null;
		SAXBuilder xmlBuilder = new SAXBuilder();

		try
		{
			// Parser le fichier donné
			document = xmlBuilder.build(this.getFichierXml());
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		catch (JDOMException e)
		{
			e.printStackTrace();
		}

		return document;
	}

	/**
	 * Retourner le root du document XML
	 * 
	 * @return Element, c'est la node root du document
	 */
	private Element getDocumentRoot()
	{
		return (this.getDocumentXml() != null) ? this.getDocumentXml()
				.getRootElement() : null;
	}
}
